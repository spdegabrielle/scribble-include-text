example
===

Example documents.

Each directory includes:

 1. One `top.scrbl` driver module
 2. One or more helper modules

To render an example, run

```
  raco scribble top.scrbl
```

And open `top.html` in a web browser.

