scribble-include-text
===

Use the contents of one file **directly** in another.

#### side.scrbl

```
This file has some text,
and calls a @emph{Scribble} function,
and references a @|variable|.
```

#### top.scrbl

```
#lang scribble/manual
@require[scribble-include-text]

@title{Example}

@(define variable "cat")

@include-text{side.scrbl}

The end.
```


#### `raco scribble top.scrbl`

![rendered top.scrbl](scribblings/example/1/top.png)


### More Examples

- `scribblings/example/`


### Thank you

Thanks to Hendrik Boom for inspiration and to Matthew Flatt for implementation:

<https://groups.google.com/g/racket-users/c/bjXmDyGhXyM/m/qaYH_owMAQAJ>

